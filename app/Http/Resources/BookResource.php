<?php

namespace App\Http\Resources;

use App\Book;
use App\Goal;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Storage;

/**
 * Class BookResource
 * @package App\Http\Resources
 * @mixin Book
 */
class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isPassed = false;

        if ($this->poll) {
            $pollId = $this->poll->id;

            $user = Auth::user();
            $isPassed = $user->successfulPolls()->where('poll_id', $pollId)->exists();
        }

        return [
            'id' => $this->id,
            'title' => $this->title,
            'author' => $this->author,
            'category' => $this->category,
            'description' => $this->description,
            'fragment' => $this->fragment,
            'audio' => $this->audio ?? null,
            'text' => $this->text ?? null,
            'link' => $this->link,
            'cover' => $this->getFirstMedia('cover')->getFullUrl('small'),
            'background' => $this->getFirstMedia('background')->getFullUrl('small'),
            'goal' => new GoalResource($this->whenLoaded('goal')),
            'is_passed' => $isPassed
        ];
    }
}
