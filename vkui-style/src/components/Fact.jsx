import React, { useEffect, useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import useGlobal from '../hooks/use-global';
import { interpretResponse } from '../utils/data';
import Title from '../components/Title';
import { usePlatform, ANDROID } from '@vkontakte/vkui';

const Fact = () => {
  const [fact, updateFact] = useState(null);
  const global = useGlobal();
  const platform = usePlatform();

  useEffect(() => {
    global.axios.get('/vk/fact').then((response) => {
      window.requestAnimationFrame(() => {
        updateFact(interpretResponse(response));
      });
    });
  }, []);

  return (
    <CSSTransition in={Boolean(fact)} classNames="fade" timeout={platform === ANDROID ? 600 : 1200}>
      {
        fact ? (
          <div className="Fact">
            <Title size="xl">Рандомный факт</Title>
            <div className="FactBanner">
              <img src={fact.img_url} alt="" className="FactBanner__image" />
              <div className="FactBanner__desc">
                {fact.content}
              </div>
            </div>
          </div>
        ) : (
          <div className="Fact" />
        )
      }
    </CSSTransition>
  );
};

export default Fact;
