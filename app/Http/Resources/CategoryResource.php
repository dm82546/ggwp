<?php

namespace App\Http\Resources;


use App\Poll;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'is_active' => $this->is_active,
            'img_url' => $this->getImage(),
            'progress' => Auth::user()->successfulPolls()->where('category_id', $this->id)->count() / Poll::where('category_id', $this->id)->count() * 100
        ];
    }
}
