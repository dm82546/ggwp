<?php

namespace App\Http\Resources;

use App\Goal;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

/**
 * Class GoalResource
 * @package App\Http\Resources
 * @mixin Goal
 */
class GoalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'deadline_at' => (string)$this->deadline_at,
            'days' => $this->deadline_at->diffInDays(Carbon::now(), true),
            'created_at' => (string)$this->created_at
        ];
    }
}
