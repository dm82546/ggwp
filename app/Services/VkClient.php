<?php


namespace App\Services;


use VK\Client\VKApiClient;

class VkClient {
    protected $client;
    private $accessToken;

    private const API_VERSION = '5.103';

    public function __construct()
    {
        $this->client = new VKApiClient(self::API_VERSION, 'ru');
        $this->accessToken = config('services.vk.app.service');
    }

    public function getUsers($ids, array $fields) {

        $isFew = is_array($ids);

        $response = $this->client->users()->get($this->accessToken, [
            'user_ids' => $isFew ? $ids : [$ids],
            'fields' => $fields,
        ]);

        return $isFew ? $response : $response[0];
    }

    public function getFriends($userId, $fields): array
    {
        try {
            $response = $this->client->friends()->get($this->accessToken, [
                'user_id' => $userId,
                'fields' => $fields
            ]);
            $users = $response['items'];
        } catch (\Exception $e) {
            $users = [];
        }

        return $users;
    }
}
