<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Goal
 *
 * @property int $id
 * @property int $user_id
 * @property int $book_id
 * @property \Illuminate\Support\Carbon $deadline_at
 * @property string $uuid
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Goal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Goal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Goal query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Goal whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Goal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Goal whereDeadlineAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Goal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Goal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Goal whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Goal whereUuid($value)
 * @mixin \Eloquent
 */
class Goal extends Model
{
    protected $fillable = [
        'book_id',
        'user_id',
        'uuid',
        'deadline_at'
    ];

    protected $dates = [
        'deadline_at'
    ];
}
