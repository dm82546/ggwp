[<img width="134" src="https://vk.com/images/apps/mini_apps/vk_mini_apps_logo.svg">](https://vk.com/services)

# @eolme/vk-mini-app-template

This is the official base template for [Create VK Mini App](https://github.com/eolme/create-vk-mini-app).

If you don't specify a template, this template will be used by default.

For more information, please refer to:

- [Getting Started](https://github.com/eolme/create-vk-mini-app/blob/master/README.md#creating-an-app) – How to create a new app.
- [User Guide](https://github.com/eolme/create-vk-mini-app/blob/master/README.md#user-guide) – CLI usage.
