<?php

Route::post('auth', 'MeController@me');

Route::prefix('categories')->group(function () {
    Route::get('/', 'CategoryController@index');
    Route::get('/{category}', 'CategoryController@getPolls');
});

Route::prefix('polls')->group(function () {
    Route::get('/{poll}', 'PollController@getQuestions');
    Route::post('/{poll}/verify', 'PollController@verifyAnswers');
});

Route::prefix('books')->group(function() {
    Route::get('/', 'BookController@index');
    Route::get('/{book}', 'BookController@show');
    Route::get('/{book}/poll', 'BookController@poll');
    Route::post('/{book}/goal', 'BookController@goal');
});

Route::get('/fact', 'FactController@getRandom');

Route::post('notifications', 'MeController@setNotificationsAreEnabled');
