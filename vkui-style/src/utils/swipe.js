import bridge from '@vkontakte/vk-bridge';

const prevent = (e = window.event) => {
  if (!e) {
    return;
  }

  if (e.cancelable) {
    if (e.preventDefault && !e.defaultPrevented) {
      e.preventDefault();
    } else {
      e.returnValue = false;
    }
  }

  if (e.stopPropagation) {
    e.stopPropagation();
  }

  return false;
};
const param = { passive: false };
const events = ['dragstart', 'dragenter', 'gesturestart', 'gesturechange', 'MSGestureStart'];

export const disable = () => {
  if (bridge.supports('VKWebAppDisableSwipeBack')) {
    bridge.send('VKWebAppDisableSwipeBack');
  }

  events.forEach((event) => {
    document.addEventListener(event, prevent, param);
  });
};

export const enable = () => {
  if (bridge.supports('VKWebAppEnableSwipeBack')) {
    bridge.send('VKWebAppEnableSwipeBack');
  }

  events.forEach((event) => {
    document.removeEventListener(event, prevent, param);
  });
};
