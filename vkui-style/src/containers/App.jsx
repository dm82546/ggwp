import React, { useEffect } from 'react';
import { ConfigProvider, Root } from '@vkontakte/vkui';

import Home from '../views/Home';
import useGlobal from '../hooks/use-global';
import { interpretResponse } from '../utils/data';

const App = () => {
  const global = useGlobal();

  useEffect(() => {
    global.axios.post('/vk/auth').then((response) => {
      global.store.user = interpretResponse(response);
      global.bus.emit('app:auth', global.store.user);
    });
  }, []);

  return (
    <ConfigProvider>
      <Root activeView="home">
        <Home id="home" />
      </Root>
    </ConfigProvider>
  );
};

export default App;
