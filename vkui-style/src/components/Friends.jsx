import React, { useState, useEffect } from 'react';
import { CSSTransition } from 'react-transition-group';
import useGlobal from '../hooks/use-global';
import { usePlatform, ANDROID } from '@vkontakte/vkui';

const Friends = () => {
  const [friends, updateFriends] = useState(null);
  const global = useGlobal();
  const platform = usePlatform();

  useEffect(() => {
    const prettyUpdateFriends = () => {
      window.requestAnimationFrame(() => {
        updateFriends(global.store.user.friends);
      });
    };

    if (global.store.user && global.store.user.friends) {
      prettyUpdateFriends();
    } else {
      global.bus.once('app:auth', prettyUpdateFriends);
    }

    return () => {
      global.bus.detach('app:auth', prettyUpdateFriends);
    };
  }, []);

  return (
    <CSSTransition in={Boolean(friends)} classNames="fade" timeout={platform === ANDROID ? 600 : 1200}>
      {
        friends && friends.length ? (
          <div className="Friends">
            <div className="Friends__desc">
              Твои друзья уже прошли тесты
            </div>
            <div className="Friends__image">
              {
                friends.slice(0, 3).map((friend) => {
                  return (<img key={friend.vk_user_id} src={friend.avatar_200} alt="" />)
                })
              }
            </div>
          </div>
        ) : (
          <div className="Friends" />
        )
      }
    </CSSTransition>
  );
};

export default Friends;
