<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * App\Poll
 *
 * @property int $id
 * @property string|null $description
 * @property int $category_id
 * @property int|null $show_time
 * @property bool $is_finished
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Category $category
 * @method static Builder|Poll newModelQuery()
 * @method static Builder|Poll newQuery()
 * @method static Builder|Poll query()
 * @method static Builder|Poll whereCategoryId($value)
 * @method static Builder|Poll whereCreatedAt($value)
 * @method static Builder|Poll whereDescription($value)
 * @method static Builder|Poll whereId($value)
 * @method static Builder|Poll whereIsFinished($value)
 * @method static Builder|Poll whereShowTime($value)
 * @method static Builder|Poll whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|Question[] $questions
 * @property-read int|null $questions_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @property-read Collection|Media[] $media
 * @property-read int|null $media_count
 * @property int $book_id
 * @property-read \App\Book $book
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Poll whereBookId($value)
 */
class Poll extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'description',
        'show_time'
    ];

    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'successful_polls');
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'poll_id');
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('part_poll')
            ->singleFile()
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('small')
                    ->crop(Manipulations::CROP_CENTER, 360, 200);

                $this
                    ->addMediaConversion('medium')
                    ->crop(Manipulations::CROP_CENTER, 710, 380);
            });
    }

    public function getImage()
    {
        return $this->getFirstMedia('part_poll')->getFullUrl();
    }
}
