import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Panel, Div } from '@vkontakte/vkui';
import Header from '../components/Header';
import Button from '../components/Button';
import Person from '../components/Person';
import Text from '../components/Text';

const Intro = ({ id, callback }) => {
  return (
    <Panel id={id} separator={false} className="Panel--flex">
      <Header />
      <Person />
      <Div>
        <Text className="Layout--space-bottom">
          Выбираешь квадрат и проходишь тест для закрепления материала.
          За пройденный тест ты получишь 1 из 4 частей картинки.
          Пройди все тесты, чтобы открыть картинку!
        </Text>
        <Button onClick={callback} data-to="home" className="Layout--bottom">Поехали</Button>
      </Div>
    </Panel>
  );
};

Intro.propTypes = {
  id: PropTypes.string.isRequired,
  callback: PropTypes.func.isRequired
};

export default memo(Intro);
