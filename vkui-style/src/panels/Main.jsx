import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Panel, Div, usePlatform, ANDROID } from '@vkontakte/vkui';
import Header from '../components/Header';
import Button from '../components/Button';
import Fact from '../components/Fact';
import Friends from '../components/Friends';
import Categories from '../components/Categories';
import { CSSTransition } from 'react-transition-group';
import useGlobal from '../hooks/use-global';

const Main = ({ id, callback }) => {
  const global = useGlobal();
  const platform = usePlatform();
  const [showCallback, updateShowCallback] = useState(false);

  const forceShowCallback = useCallback(() => {
    window.requestAnimationFrame(() => {
      updateShowCallback(true);
    });
  }, [updateShowCallback]);

  useEffect(() => {
    const checkShowCallback = () => {
      if (global.store.selected !== -1) {
        forceShowCallback();
      }
    };

    checkShowCallback();

    global.bus.on('app:select', checkShowCallback);
    return () => {
      global.bus.detach('app:select', checkShowCallback);
    };
  }, []);

  return (
    <Panel id={id} separator={false} className="Panel--flex">
      <Header />
      <Div>
        <Fact />
        <Friends />
        <Categories />
        <CSSTransition in={Boolean(showCallback)} classNames="fade" timeout={platform === ANDROID ? 600 : 1200}>
          <Button onClick={callback} data-to="quiz" className="Layout--bottom">Начать тестер</Button>
        </CSSTransition>
      </Div>
    </Panel>
  );
};

Main.propTypes = {
  id: PropTypes.string.isRequired,
  callback: PropTypes.func.isRequired
};

export default Main;
