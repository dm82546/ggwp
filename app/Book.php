<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;


/**
 * App\Book
 *
 * @property int $id
 * @property string $title
 * @property string $author
 * @property string $category
 * @property string $description
 * @property string $fragment
 * @property string $link
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Media[] $media
 * @property-read int|null $media_count
 * @property-read Collection|Poll[] $polls
 * @property-read int|null $polls_count
 * @method static Builder|Book newModelQuery()
 * @method static Builder|Book newQuery()
 * @method static Builder|Book query()
 * @method static Builder|Book whereAuthor($value)
 * @method static Builder|Book whereCategory($value)
 * @method static Builder|Book whereCreatedAt($value)
 * @method static Builder|Book whereDescription($value)
 * @method static Builder|Book whereFragment($value)
 * @method static Builder|Book whereId($value)
 * @method static Builder|Book whereLink($value)
 * @method static Builder|Book whereTitle($value)
 * @method static Builder|Book whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read \App\Goal $goal
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Goal[] $goals
 * @property-read int|null $goals_count
 * @property-read \App\Poll $poll
 * @property string|null $audio
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereAudio($value)
 * @property string|null $text
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereText($value)
 */
class Book extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'title',
        'author',
        'category',
        'description',
        'fragment',
        'link',
        'audio',
        'text'
    ];

    public function poll()
    {
        return $this->hasOne(Poll::class);
    }

    public function goals()
    {
        return $this->hasMany(Goal::class);
    }

    public function goal()
    {
        return $this->hasOne(Goal::class);
    }


    public function registerMediaCollections()
    {
        $this->addMediaCollection('cover')
            ->singleFile()
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('small')
                    ->crop(Manipulations::CROP_TOP, 500 * 2, 430 * 2);
            });

        $this->addMediaCollection('background')
            ->singleFile()
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('small')
                    ->crop(Manipulations::CROP_TOP, 630 * 2, 545 * 2);
            });
    }
}
