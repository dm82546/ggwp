<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Http\Requests\VerifyAnswersRequest;
use App\Http\Resources\QuestionResource;
use App\Poll;
use App\Question;
use App\Vote;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class PollController extends Controller
{
    public function getQuestions(Poll $poll)
    {
        return QuestionResource::collection($poll->questions);
    }

    public function verifyAnswers(VerifyAnswersRequest $request, Poll $poll)
    {
        $user = Auth::user();

        $result = self::verifyAnswersService($poll, $request->input('answers'));

        if (!$result) {
            abort(403);
        } else {
            if (!$user->successfulPolls()->where('poll_id', $poll->id)->exists()) {
                $user->successfulPolls()->attach($poll->id);
            }
        }
    }

    private static function verifyAnswersService($vote, $result): bool
    {
        $collection = new Collection($result);

        $answers = $collection->mapWithKeys(function ($value) {
            return [
                $value['question_id'] => $value['answer_id']
            ];
        })->sortKeys();

        return self::getRealAnswers($vote)->diffAssoc($answers)->count() === 0;
    }

    private static function getRealAnswers(Poll $vote): Collection
    {
        return $vote->questions->mapWithKeys(function (Question $question) {
            $answer = $question->answers->first(function (Answer $answer) {
                return $answer->is_right;
            });

            return [
                $question->id => $answer->id
            ];
        })->sortKeys();
    }
}
