import React from 'react';
import PropTypes from 'prop-types';
import cn from 'clsx';

const Text = (props) => {
  return (
    <div {...props} className={cn('Text', props.className)}>
      {props.children}
    </div>
  );
}

Text.propTypes = {
  children: PropTypes.node,
  className: PropTypes.any
};

export default Text;
