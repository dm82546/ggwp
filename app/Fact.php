<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * App\Fact
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fact newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fact newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fact query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fact whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fact whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fact whereUpdatedAt($value)
 */
class Fact extends Model implements HasMedia
{

    use HasMediaTrait;

    protected $fillable = [
        'content'
    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('fact_poster')
            ->singleFile()
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('small')
                    ->crop(Manipulations::CROP_CENTER, 360, 200);

                $this
                    ->addMediaConversion('medium')
                    ->crop(Manipulations::CROP_CENTER, 710, 380);
                $this
                    ->addMediaConversion('thumb')
                    ->crop(Manipulations::CROP_CENTER, 400, 400);
            });
    }

    public function getPoster() {
        return $this->getFirstMedia('fact_poster')->getFullUrl('thumb');
    }
}
