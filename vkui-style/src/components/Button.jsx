import React from 'react';
import PropTypes from 'prop-types';
import { Button as NativeButton } from '@vkontakte/vkui';
import cn from 'clsx';

const Button = (props) => {
  return (
    <NativeButton {...props} size="xl" className={cn('Button--round', props.className)}>
      {props.children}
    </NativeButton>
  );
}

Button.propTypes = {
  children: PropTypes.node,
  className: PropTypes.any,
  onClick: PropTypes.func
};

export default Button;
