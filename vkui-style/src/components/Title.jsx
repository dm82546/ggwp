import React, { memo } from 'react';
import PropTypes from 'prop-types';
import cn from 'clsx';

const Title = (props) => {
  return (
    <h1 className={cn('Title', props.size && `Title--${props.size}`)}>
      {props.children}
    </h1>
  );
};

Title.defaultProps = {
  size: false
};

Title.propTypes = {
  children: PropTypes.node,
  size: PropTypes.any
};

export default memo(Title);
