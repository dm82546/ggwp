<?php

use App\Category;
use Illuminate\Database\Seeder;

class PollsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $book = \App\Book::findOrFail(1);

        /**
         * @var \App\Poll $poll
         */
        $poll = $book->poll()->create([
            'description' => "desc",
            'show_time' => 120
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Что Брайан Трейси называет первым правилом борьбы с потерянным временем?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Выбор самой главной цели',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Разбиение задачи на подзадачи',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Ведение ежедневника',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Хорошее настроение ',
            'is_right' => false
        ]);


        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Что является главным отличием успешного человека?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Склад ума',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Наличие способностей',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Постановка долгосрочных целей',
            'is_right' => true
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Какой метод лучше всего помогает ставить правильные цели?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'ABCDE',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Шкала от 1 до 10',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Диаграммный метод',
            'is_right' => false
        ]);


        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Что помогает работать продуктивнее?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Чашка кофе перед работой',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Комфортная рабочая обстановка',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Музыка в наушниках',
            'is_right' => false
        ]);

        $book = \App\Book::findOrFail(2);

        /**
         * @var \App\Poll $poll
         */
        $poll = $book->poll()->create([
            'description' => "desc",
            'show_time' => 120
        ]);

        /**
         * @var \App\Question $question
         */
//        $question = $poll->questions()->create([
//            'question' => 'Что такое мыслетопливо?',
//            'description' => 'desc'
//        ]);
//        $question->answers()->create([
//            'answer' => 'Энергия которая помогает нам сохранять осознанность',
//            'is_right' => true
//        ]);
//        $question->answers()->create([
//            'answer' => 'Разбиение задачи на подзадачи',
//            'is_right' => false
//        ]);
//        $question->answers()->create([
//            'answer' => 'Ведение ежедневника',
//            'is_right' => false
//        ]);
//        $question->answers()->create([
//            'answer' => 'Хорошее настроение ',
//            'is_right' => false
//        ]);


        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Что нужно сделать, чтобы быстрее и успешнее завершить большую задачу?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Разделить её на более мелкие задачи',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Делегировать',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Отложить на более позднее время',
            'is_right' => false
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Как называются виды нашей памяти?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Долгосрочная и скоротечная',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Долгосрочная и рабочая',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Рабочая и скоротечная',
            'is_right' => false
        ]);



        $book = \App\Book::findOrFail(3);

        /**
         * @var \App\Poll $poll
         */
        $poll = $book->poll()->create([
            'description' => "desc",
            'show_time' => 120
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Назовите один из основных признаков непрофессионального лидера.',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Крик на подчиненных',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Излишняя доброта',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Неумение найти подход к каждому',
            'is_right' => false
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Что необходимо сделать для повышения качества работы?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Взять себе помощника',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Упростить процессы',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Увеличить штат сотрудников',
            'is_right' => false
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Что из перечисленного “съедает” больше всего времени?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Пустые многолюдные собрания',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Неправильная расстановка задач',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Неправильное распределение ресурсов',
            'is_right' => false
        ]);



        $book = \App\Book::findOrFail(4);

        /**
         * @var \App\Poll $poll
         */
        $poll = $book->poll()->create([
            'description' => "desc",
            'show_time' => 120
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'С чем нужно определиться в первую очередь перед написанием текста?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'С целью текста',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'С объемом текста',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'С жанром текста',
            'is_right' => false
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Что самое главное в сильном тексте?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Учесть все свои пожелания',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Забота о читателе',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Количество информации',
            'is_right' => false
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Как называются слова, нежелательные в использовании сильных текстов?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Стоп-слова',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Слова-смыслоеды',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Бичовки',
            'is_right' => false
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Сколько мыслей Максим Ильяхов советует вкладывать в одно предложение?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Не больше одной',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Не больше двух',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Не больше четырех',
            'is_right' => false
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Как проще понять, где есть лишние слова?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'Прочитать текст вслух',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'Прослушать текст',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'Дать прочитать текст знакомым',
            'is_right' => false
        ]);




        $book = \App\Book::findOrFail(5);

        /**
         * @var \App\Poll $poll
         */
        $poll = $book->poll()->create([
            'description' => "desc",
            'show_time' => 120
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Что подразумевается под термином “крысиные бега” ?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'средний класс, жаждущий обогатиться',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'бесконечная рутина работы на всех, кроме себя',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'погоня за недостижимой целью',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'ходи в школу, учись хорошо, найди хорошую работу',
            'is_right' => false
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Объясните почему один из советов богатого папы заключается в том, что человеку нужно как можно чаще менять место работы.',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'необходимо расширять свой кругозор и специализацию',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'стабильность - это признак регресса',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'чем больше навыков, тем легче жить',
            'is_right' => false
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'В чем заключается правило денег 90/10?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => '90 процентов нашего успеха создаются 10 процентами наших усилий',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => '10 процентов людей владеют 90 процентами всех денег',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => '10 процентов с доходов необходимо откладывать на черный день',
            'is_right' => false
        ]);

        /**
         * @var \App\Question $question
         */
        $question = $poll->questions()->create([
            'question' => 'Как думают успешные инвесторы?',
            'description' => 'desc'
        ]);
        $question->answers()->create([
            'answer' => 'только богатые могут рисковать',
            'is_right' => false
        ]);
        $question->answers()->create([
            'answer' => 'только бизнес сможет сделать вас богатым',
            'is_right' => true
        ]);
        $question->answers()->create([
            'answer' => 'лишь образованный может стать богатым',
            'is_right' => false
        ]);
    }
}
