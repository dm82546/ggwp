<?php

namespace App\Http\Controllers;

use App\Fact;
use App\Http\Resources\FactResource;

class FactController extends Controller
{
    public function getRandom() {
        $fact = Fact::inRandomOrder()->first();
        return new FactResource($fact);
    }
}
