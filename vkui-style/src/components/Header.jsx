import React from 'react';
import PropTypes from 'prop-types';
import { PanelHeaderSimple, PanelHeaderBack, PanelHeaderClose } from '@vkontakte/vkui';

import Logo from './Logo';

const Header = ({ showClose, showBack }) => {
  const icon =
    showClose && (<PanelHeaderClose />) ||
    showBack && (<PanelHeaderBack />);

  return (
    <PanelHeaderSimple left={icon} separator={false}>
      <Logo />
    </PanelHeaderSimple>
  );
};

Header.defaultProps = {
  showClose: false,
  showBack: false
};

Header.propTypes = {
  showClose: PropTypes.bool,
  showBack: PropTypes.bool
};

export default Header;
