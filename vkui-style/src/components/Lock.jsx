import React from 'react';

const Lock = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="70" height="72" fill="none" className="Lock">
      <path stroke="#9ae3da" strokeWidth="4" d="M68 49.84v-25a8 8 0 00-8-8H10a8 8 0 00-8 8v25a8 8 0 004.73 7.3l25 11.2a8 8 0 006.54 0l25-11.2a8 8 0 004.73-7.3z" />
      <path fill="#9ae3da" fillRule="evenodd" d="M37.27 40.48a.26.26 0 01.15-.27A4.33 4.33 0 0040 36.37c0-2.41-2.24-4.37-5-4.37s-5 1.96-5 4.38a4.33 4.33 0 002.58 3.83c.1.04.16.16.15.27L31.3 49.2c-.03.15.09.29.24.29h6.92c.15 0 .27-.14.24-.29l-1.43-8.73z" clipRule="evenodd" />
      <path stroke="#9ae3da" strokeWidth="4" d="M16.1 14.84V10a8 8 0 018-8h21.8a8 8 0 018 8v4.84" />
    </svg>
  );
};

export default Lock;
