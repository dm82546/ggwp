<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\PollResource;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function index() {
        return CategoryResource::collection(Category::all());
    }

    public function getPolls(Category $category) {
        return PollResource::collection($category->polls);
    }
}
