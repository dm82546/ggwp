<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class PollResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'show_time' => $this->show_time,
            'is_finished' => Auth::user()->successfulPolls()->where('poll_id', $this->id)->exists(),
            'questions' => QuestionResource::collection($this->questions)
        ];
    }
}
