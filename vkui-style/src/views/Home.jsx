import React, { useCallback } from 'react';
import { View } from '@vkontakte/vkui';
import usePanels from '../hooks/use-panels';
import useGlobal from '../hooks/use-global';
import context from '../utils/context';

import Intro from '../panels/Intro';
import Main from '../panels/Main';
import Quiz from '../panels/Quiz';

const isFirstTime = context.$storage.syncUniversal('first', true);
const initialPanel = isFirstTime ? 'intro' : 'main';

const Home = () => {
  const panels = usePanels(initialPanel);
  const global = useGlobal();

  const reset = useCallback(() => {
    panels.setHistory(() => {
      const resetFirstTime = global.storage.getUniversal('first');
      const resetInitialPanel = resetFirstTime ? 'intro' : 'main';
      panels.setActivePanel(resetInitialPanel, true);
      return [resetInitialPanel];
    });
  }, []);

  const accept = useCallback(() => {
    global.storage.setUniversal('first', false);
    reset();
  }, [reset]);

  return (
    <View activePanel={panels.activePanel} history={panels.history} onSwipeBack={panels.goBack} header={false}>
      <Intro id="intro" callback={accept} />
      <Main id="main" callback={panels.goForward} />
      <Quiz id="quiz" callback={reset} />
    </View>
  );
};

export default Home;
