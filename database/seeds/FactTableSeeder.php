<?php

use App\Fact;
use Illuminate\Database\Seeder;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig;

class FactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws DiskDoesNotExist
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function run()
    {
        $json_facts = storage_path('/app/seed/facts.json');
        $json_data = file_get_contents($json_facts);
        $newFacts = json_decode($json_data, true);

        foreach($newFacts as $newFact) {

            if (mb_strlen($newFact['content']) > 55) {
                continue;
            }

            Fact::create([
                'content' => $newFact['content']
            ]);
        }


        Fact::create([
            'content' => "43% молодежи 13-18 лет со всего мира всюду носят с собой наушники"
        ]);
    }
}
