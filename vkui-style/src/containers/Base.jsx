import React, { useState, useEffect } from 'react';

import App from './App';
import Offline from '../components/Offline';

import bridge from '@vkontakte/vk-bridge';
import '@vkontakte/vkui/dist/vkui.css';
import '../styles/style.css';
import useGlobal from '../hooks/use-global';

const Base = () => {
  const [showOffline, setShowOffline] = useState(false);
  const global = useGlobal();

  useEffect(() => {
    const handleOnlineStatus = () => {
      global.store.isOffline = !window.navigator.onLine;
      window.requestAnimationFrame(() => {
        setShowOffline(global.store.isOffline);
      });
    };

    window.addEventListener('online', handleOnlineStatus);
    window.addEventListener('offline', handleOnlineStatus);
  }, []);

  useEffect(() => {
    bridge.send('VKWebAppInit');
    if (bridge.supports('VKWebAppSetViewSettings')) {
      bridge.send('VKWebAppSetViewSettings', {
        status_bar_style: 'light',
        action_bar_color: '#02B9A3',
        navigation_bar_color: '#fff'
      });
    }
  }, []);

  return (
    <>
      <App />
      <Offline visible={showOffline} />
    </>
  );
};

export default Base;
