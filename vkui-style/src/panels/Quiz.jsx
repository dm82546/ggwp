import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Panel, PanelSpinner, Div } from '@vkontakte/vkui';
import bridge from '@vkontakte/vk-bridge';
import Header from '../components/Header';
import Button from '../components/Button';
import Title from '../components/Title';
import Text from '../components/Text';
import Lock from '../components/Lock';
import useGlobal from '../hooks/use-global';
import { interpretResponse } from '../utils/data';

const Quiz = ({ id, callback }) => {
  const global = useGlobal();
  const [isLoadingState, updateLoadingState] = useState(true);
  const [state, updateState] = useState({
    isAvailable: true,

    isOpen0: false,
    isOpen1: false,
    isOpen2: false,
    isOpen3: false,

    stage: -1,
    question: -1,
    bet: 0
  });

  useEffect(() => {
    const prettyUpdateLoadingState = () => {
      window.requestAnimationFrame(() => {
        updateLoadingState(false);
      });
    };

    const selected = global.store.selected;
    const category = global.store.categories[selected];

    global.axios.get(`/vk/categories/${category.id}`).then((response) => {
      global.store.cache[category.id] = interpretResponse(response);

      const quiz = global.store.cache[category.id];

      const isOpen0 = quiz[0].is_finished;
      const isOpen1 = quiz[1].is_finished;
      const isOpen2 = quiz[2].is_finished;
      const isOpen3 = quiz[3].is_finished;

      updateState({
        ...state,

        isOpen0,
        isOpen1,
        isOpen2,
        isOpen3
      });
      prettyUpdateLoadingState();
    }).catch(() => {
      updateState({
        isAvailable: false,

        isOpen0: false,
        isOpen1: false,
        isOpen2: false,
        isOpen3: false,

        stage: -1,
        question: -1,
        bet: 0
      });
      prettyUpdateLoadingState();
    });
  }, []);

  const submitQuiz = useCallback((id, result) => {
    global.axios.post(`/vk/polls/${id}/verify`, { result }).catch(() => {
      // ignore
    });
  }, []);

  const showImage = useCallback((url) => {
    if (bridge.supports('VKWebAppShowImages')) {
      bridge.send('VKWebAppShowImages', {
        images: [url]
      });
    } else {
      window.open(url, '_blank');
    }
  }, []);

  const prettyUpdateState = useCallback((payload) => {
    window.requestAnimationFrame(() => {
      updateState({
        ...state,
        ...payload
      });
    });
  }, [state, updateState]);

  let view = null;
  if (isLoadingState) {
    view = (<PanelSpinner />);
  } else if (!state.isAvailable) {
    view = (
      <>
        <Title>Locked category</Title>
        <Text>
          В данной категории мы расскажем вам о строении человеческого тела.
          Статьи и тесты будут доступны через некоторое время.
        </Text>
        <div className="CategoryGrid">
          <div className="CategoryGrid__lock">
            <Lock />
            Скоро будет
          </div>
        </div>
      </>
    );
  } else {
    const selected = +global.store.selected;
    const category = global.store.categories[selected];
    const quiz = global.store.cache[category.id];

    if (state.stage === -1) {
      if (
        state.isOpen0 &&
        state.isOpen1 &&
        state.isOpen2 &&
        state.isOpen3
      ) {
        view = (
          <>
            <Title>Поздравляем, ты умничка!</Title>
            <Text>Ты прошел все тесты из категории &quot;{category.title}&quot;, узнал что-то новое и открыл полное изображение!</Text>
            <div className="CategoryResult" onClick={() => showImage(category.img_url)}>
              <img src={category.img_url} alt="" />
            </div>
            <Button onClick={callback} className="Layout--bottom">На главную</Button>
          </>
        );
      } else {
        view = (
          <>
            <Title>{category.title}</Title>
            <Text>{category.description}</Text>
            <div className="CategoryGrid">
              <div onClick={() => !state.isOpen0 && prettyUpdateState({ stage: 0, question: -1, bet: 0 })} className="CategoryGrid__item">
                {
                  state.isOpen0 ? (<img src={quiz[0].img_url} alt="" />) : (<span>1</span>)
                }
              </div>
              <div onClick={() => !state.isOpen1 && prettyUpdateState({ stage: 1, question: -1, bet: 0 })} className="CategoryGrid__item">
                {
                  state.isOpen1 ? (<img src={quiz[1].img_url} alt="" />) : (<span>2</span>)
                }
              </div>
              <div onClick={() => !state.isOpen2 && prettyUpdateState({ stage: 2, question: -1, bet: 0 })} className="CategoryGrid__item">
                {
                  state.isOpen2 ? (<img src={quiz[2].img_url} alt="" />) : (<span>3</span>)
                }
              </div>
              <div onClick={() => !state.isOpen3 && prettyUpdateState({ stage: 3, question: -1, bet: 0 })} className="CategoryGrid__item">
                {
                  state.isOpen3 ? (<img src={quiz[3].img_url} alt="" />) : (<span>4</span>)
                }
              </div>
            </div>
            <Button onClick={callback} className="Layout--bottom">На главную</Button>
          </>
        );
      }
    } else {
      const current = quiz[state.stage];

      if (state.question === -1) {
        view = (
          <>
            <Title>{current.title}</Title>
            <Text className="Layout--space-bottom">{current.description}</Text>
            <Button onClick={() => prettyUpdateState({ question: 0 })} className="Layout--bottom">Я готов</Button>
          </>
        );
      } else {
        const question = current.questions[state.question];
        const questionCurrent = state.question + 1;
        const questionTotal = current.questions.length;

        if (state.question >= current.questions.length) {
          const result = state.bet >= current.questions.length;
          submitQuiz(current.id, result);

          if (result) {
            view = (
              <>
                <Title>Поздравляем, ты сделал это!</Title>
                <Text>Ты прошел тест и в награду получаешь одну из 4 частей пазла. Прочтай оставшиеся статьи, чтобы полностью открыть картинку!</Text>
                <div className="CategoryResult">
                  <img src={current.img_url} alt="" />
                </div>
                <Button onClick={() => prettyUpdateState({ stage: -1, [`isOpen${state.stage}`]: true })} className="Layout--bottom">Пройти следующий тест</Button>
              </>
            );
          } else {
            view = (
              <>
                <Title>Ты можешь лучше</Title>
                <Text>К сожалению, ты ответил верно не на все вопросы. Для получения части пазла ты можешь попробовать еще раз!</Text>
                <div className="CategoryResult">
                  <span>{state.bet}/{current.questions.length}</span>
                </div>
                <Button onClick={() => prettyUpdateState({ stage: -1, [`isOpen${state.stage}`]: false })} className="Layout--bottom">Пройти тестер еще раз</Button>
              </>
            );
          }
        } else {
          const answers = question.answers.map((answer) => {
            return (
              <Button
                key={answer.id}
                onClick={() => prettyUpdateState({ question: questionCurrent, bet: state.bet + answer.is_right })}
                className="Layout--space-top"
              >{answer.answer}</Button>
            );
          });

          view = (
            <>
              <Title>Вопрос {questionCurrent}/{questionTotal}</Title>
              <Text>{question.question}</Text>
              <div className="CategoryAnswers">{answers}</div>
            </>
          );
        }
      }
    }
  }

  return (
    <Panel id={id} separator={false} className="Panel--flex">
      <Header />
      <Div>
        {view}
      </Div>
    </Panel>
  );
};

Quiz.propTypes = {
  id: PropTypes.string.isRequired,
  callback: PropTypes.func.isRequired
};

export default Quiz;
