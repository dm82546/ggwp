import context from '../utils/context';
import Bus from 'js-event-bus';
import Storage from '@eolme/universal-storage/dist/index.modern';
import axios from 'axios';
import retry from 'axios-retry';
import { parseQuery } from '../utils/uri';
import { getUTCOffset } from '../utils/date';

const axiosInstance = axios.create({
  baseURL: 'https://tttester.ezavalishin.ru',
  headers: {
    'Vk-Params': window.btoa(JSON.stringify({
      ...parseQuery(window.location.search),
      utc_offset: getUTCOffset()
    })),
    'Accept': 'application/json'
  }
});
retry(axiosInstance, { retries: 3, shouldResetTimeout: true });

context.$store = {
  isOffline: !window.navigator.onLine,
  selected: -1,
  categories: [],
  cache: {}
};
context.$storage = new Storage();
context.$bus = new Bus();
context.$axios = axiosInstance;

const provider = {
  store: context.$store,
  storage: context.$storage,
  bus: context.$bus,
  axios: context.$axios
};

export default function useGlobal() {
  return provider;
}
