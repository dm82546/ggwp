import React, { useState, useEffect } from 'react';
import { CSSTransition } from 'react-transition-group';
import { Gallery, usePlatform, ANDROID } from '@vkontakte/vkui';
import useGlobal from '../hooks/use-global';
import { interpretResponse } from '../utils/data';

const Categories = () => {
  const [categories, updateCategories] = useState(null);
  const [selected, updateSelected] = useState(0);
  const global = useGlobal();
  const platform = usePlatform();

  useEffect(() => {
    global.axios.get('/vk/categories').then((response) => {
      window.requestAnimationFrame(() => {
        global.store.categories = interpretResponse(response);
        updateCategories(global.store.categories);
      });
    });
  }, []);

  useEffect(() => {
    global.store.selected = selected;
    global.bus.emit('app:select', global.store.selected);
  }, [selected]);

  return (
    <CSSTransition in={Boolean(categories)} classNames="fade" timeout={platform === ANDROID ? 600 : 1200}>
      {
        categories ? (
          <Gallery
            slideWidth="80%"
            align="right"
            className="CategoryGallery"
            slideIndex={selected}
            onChange={slideIndex => updateSelected(slideIndex)}
          >
            {
              categories.map((category) => {
                return (
                  <div key={category.id} className="Category">
                    <div className="Category__title">
                      {category.title}
                    </div>
                    {
                      category.progress > 0 ? (
                        <div className="CategoryProgress">
                          <div className="CategoryProgress__indicator">
                            <div className="CategoryProgress__fill" style={{ width: `${category.progress}%` }} />
                          </div>
                          <div className="CategoryProgress__desc">
                            {category.progress}%
                          </div>
                        </div>
                      ) : null
                    }
                  </div>
                );
              })
            }
          </Gallery>
        ) : <div className="Gallery CategoryGallery" />
      }
    </CSSTransition>
  );
}

export default Categories;
