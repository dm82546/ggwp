<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Requests\SetGoalRequest;
use App\Http\Resources\BookResource;
use App\Http\Resources\GoalResource;
use App\Http\Resources\PollResource;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class BookController extends Controller
{
    public function index() {

        $books = Book::with(['goal' => function($q) {
            return $q->where('user_id', Auth::id());
        }])->get();

        return BookResource::collection($books);
    }

    public function poll(Book $book) {
        $poll = $book->poll;

        return new PollResource($poll);
    }

    public function show(Book $book) {

        $book->load(['goal' => function($q) {
            return $q->where('user_id', Auth::id());
        }]);

        return new BookResource($book);
    }

    public function goal(Book $book, SetGoalRequest $request) {

        $user = Auth::user();

        $deadlineAt = Carbon::now()->addDays($request->days);

        $goal = $user->goals()->firstOrCreate([
            'book_id' => $book->id
        ], [
            'uuid' => Str::uuid(),
            'deadline_at' => $deadlineAt
        ]);

        return new GoalResource($goal);
    }
}
